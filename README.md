# INSTALL

```
cp config/apps.yml.example config/apps.yml
bundle install --without test development --path vendor/bundle
bundle exec padrino rake session_key:generate
bundle exec padrino rake sq:create RACK_ENV=production
bundle exec padrino rake sq:migrate RACK_ENV=production
bundle exec padrino rake db:seed RACK_ENV=production
```

# RUN

```
bundle exec padrino s -e production
```

# cron example

```
30  *  *   *   *     cd /path/to/tenki && bundle exec padrino runner -e production scripts/cron.rb
```
