Tenki::Admin.controllers :lands do
  get :index do
    @title = "Lands"
    @lands = Land.all
    render 'lands/index'
  end

  get :new do
    @title = pat(:new_title, :model => 'land')
    @land = Land.new
    render 'lands/new'
  end

  post :create do
    @land = Land.new(params[:land])
    if (@land.save rescue false)
      @title = pat(:create_title, :model => "land #{@land.id}")
      flash[:success] = pat(:create_success, :model => 'Land')
      params[:save_and_continue] ? redirect(url(:lands, :index)) : redirect(url(:lands, :edit, :id => @land.id))
    else
      @title = pat(:create_title, :model => 'land')
      flash.now[:error] = pat(:create_error, :model => 'land')
      render 'lands/new'
    end
  end

  get :edit, :with => :id do
    @title = pat(:edit_title, :model => "land #{params[:id]}")
    @land = Land[params[:id]]
    if @land
      render 'lands/edit'
    else
      flash[:warning] = pat(:create_error, :model => 'land', :id => "#{params[:id]}")
      halt 404
    end
  end

  put :update, :with => :id do
    @title = pat(:update_title, :model => "land #{params[:id]}")
    @land = Land[params[:id]]
    if @land
      if @land.modified! && @land.update(params[:land])
        flash[:success] = pat(:update_success, :model => 'Land', :id =>  "#{params[:id]}")
        params[:save_and_continue] ?
          redirect(url(:lands, :index)) :
          redirect(url(:lands, :edit, :id => @land.id))
      else
        flash.now[:error] = pat(:update_error, :model => 'land')
        render 'lands/edit'
      end
    else
      flash[:warning] = pat(:update_warning, :model => 'land', :id => "#{params[:id]}")
      halt 404
    end
  end

  delete :destroy, :with => :id do
    @title = "Lands"
    land = Land[params[:id]]
    if land
      if land.destroy
        flash[:success] = pat(:delete_success, :model => 'Land', :id => "#{params[:id]}")
      else
        flash[:error] = pat(:delete_error, :model => 'land')
      end
      redirect url(:lands, :index)
    else
      flash[:warning] = pat(:delete_warning, :model => 'land', :id => "#{params[:id]}")
      halt 404
    end
  end

  delete :destroy_many do
    @title = "Lands"
    unless params[:land_ids]
      flash[:error] = pat(:destroy_many_error, :model => 'land')
      redirect(url(:lands, :index))
    end
    ids = params[:land_ids].split(',').map(&:strip)
    lands = Land.where(:id => ids)
    
    if lands.destroy
    
      flash[:success] = pat(:destroy_many_success, :model => 'Lands', :ids => "#{ids.to_sentence}")
    end
    redirect url(:lands, :index)
  end
end
