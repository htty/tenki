require 'spec_helper'

describe "Tenki::App::TenkiHelper" do
  let(:helpers){ Class.new }
  before { helpers.extend Tenki::App::TenkiHelper }
  subject { helpers }

  it "should return nil" do
    expect(subject.foo).to be_nil
  end
end
