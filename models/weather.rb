class Weather < Sequel::Model
  many_to_one :land

  def Weather.cache_or_find(land_id, t = Time.now)
    w = Weather.find({land_id: land_id, year: t.year, month: t.month, day: t.day, hour: t.hour})
    w = Tenki.jma2weather(land_id) unless w
    w
  end

  def tenki_s
    #ファイル名から天気の文字列を返す
=begin
100:昼−晴れ
101:昼−晴れ　時々　曇り
102:昼−晴れ　時々　雨
104:昼−晴れ　時々　雪
110:昼−晴れ　のち　曇り
112:昼−晴れ　のち　雨
115:昼−晴れ　のち　雪
200:昼−曇り
201:昼−曇り　時々　晴れ
202:昼−曇り　時々　雨
204:昼−曇り　時々　雪
210:昼−曇り　のち　晴れ
212:昼−曇り　のち　雨
215:昼−曇り　のち　雪
300:昼−雨
301:昼−雨　時々　晴れ
302:昼−雨　時々　曇り
303:昼−雨　時々　雪
308:暴風雨
311:昼−雨　のち　晴れ
313:昼−雨　のち　曇り
314:昼−雨　のち　雪
400:昼−雪
401:昼−雪　時々　晴れ
402:昼−雪　時々　曇り
403:昼−雪　時々　雨
411:昼−雪　のち　晴れ
413:昼−雪　のち　曇り
414:昼−雪　のち　雨
700:夜−晴れ
701:夜−晴れ　時々　曇り
702:夜−晴れ　時々　雨
703:夜−晴れ　時々　雪
704:夜−曇り　時々　晴れ
705:夜−雨　時々　晴れ
706:夜−雪　時々　晴れ
707:夜−晴れ　のち　曇り
708:夜−晴れ　のち　雨
709:夜−晴れ　のち　雪
710:夜−曇り　のち　晴れ
711:夜−雨　のち　晴れ
712:夜−雪　のち　晴れ
=end
    case self.weather
    when 100,700
      "晴"
    when 101,701
      "晴/曇"
    when 102,702
      "晴/雨"
    when 104,703
      "晴/雪"
    when 110,707
      "晴>曇"
    when 112,708
      "晴>雨"
    when 115,709
      "晴>雪"
    when 200
      "曇"
    when 201,704
      "曇/晴"
    when 202
      "曇/雨"
    when 204
      "曇/雪"
    when 210,710
      "曇>晴"
    when 212
      "曇>雨"
    when 215
      "曇>雪"
    when 300
      "雨"
    when 301,705
      "雨/晴"
    when 302
      "雨/曇"
    when 303
      "雨/雪"
    when 308
      "暴風雨"
    when 311,711
      "雨>晴"
    when 313
      "雨>曇"
    when 314
      "雨>雪"
    when 400
      "雪"
    when 401,706
      "雪/晴"
    when 402
      "雪/曇"
    when 403
      "雪/雨"
    when 411,712
      "雪>晴"
    when 413
      "雪>曇"
    when 414
      "雪>雨"
    else
      code.to_s
    end
  end

  def temp_s
    [self.mintemp, self.maxtemp].map{|temp|
      temp ? temp.to_s : "--"}.join("/") + "度"
  end

  def rain_s
    rains.map{|rain|
      rain ? rain.to_s : "--"}.join("%/") + "%"
  end

  def rains
    [self.rain0006, self.rain0612, self.rain1218, self.rain1824]
  end

  def max_rain
    rains.map{|x| x.to_i}.max
  end

end
