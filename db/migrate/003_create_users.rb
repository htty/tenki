Sequel.migration do
  up do
    create_table :users do
      primary_key :id
      String :mail
      Int :hour
      Int :rain_threshold, default: 0
      foreign_key :land_id, :lands

      index :hour
      unique :mail
    end
  end

  down do
    drop_table :users
  end
end
