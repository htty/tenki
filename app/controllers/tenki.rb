Tenki::App.controllers :tenki do
  
  get '/' do
    render :index
  end

  get '/:mail' do
    user = User.find({:mail => params[:mail]})

    @mail = params[:mail]
    @land_id = user ? user.land_id : 3460
    @hour = user ? user.hour : -1
    @rain_threshold = user ? user.rain_threshold : 0

    render :index
  end
  
  post '/update' do
    user = User.find({:mail => params[:mail]})
    user = User.new({:mail => params[:mail]}) unless user
    user.hour = params[:hour]
    user.land_id = params[:land]
    user.rain_threshold = params[:rain_threshold]
    user.save

    user.delete if user.hour == -1

    redirect "/tenki/#{CGI.escape params[:mail]}"
  end

end
