Sequel.migration do
  up do
    create_table :weathers do
      primary_key :id
      Int :year
      Int :month
      Int :day
      Int :hour
      Int :weather
      Int :rain0006
      Int :rain0612
      Int :rain1218
      Int :rain1824
      Int :mintemp
      Int :maxtemp
      DateTime :last_modified
      Text :raw
      foreign_key :land_id, :lands

      index [:land_id, :year, :month, :day, :hour], unique: true
    end
  end

  down do
    drop_table :weathers
  end
end
