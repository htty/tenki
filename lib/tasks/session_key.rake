namespace :session_key do
  desc "Generate session key file"
  task :generate do
    open("./config/session_key", "w").write(SecureRandom.hex(32))
  end
end
