class User < Sequel::Model
  many_to_one :land

  def weather
    Weather.cache_or_find(self.land_id)
  end
end
