# Seed add you the ability to populate your db.
# We provide you a basic shell for interaction with the end user.
# So try some code like below:
#
#   name = shell.ask("What's your name?")
#   shell.say name
#
email     = shell.ask "Which email do you want use for logging into admin?"

unless email == ""
  password  = shell.ask "Tell me the password to use:"

  shell.say ""

  account = Account.create(:email => email, :name => "Foo", :surname => "Bar", :password => password, :password_confirmation => password, :role => "admin")
  
  if account.valid?
    shell.say "================================================================="
    shell.say "Account has been successfully created, now you can login with:"
    shell.say "================================================================="
    shell.say "   email: #{email}"
    shell.say "   password: #{password}"
    shell.say "================================================================="
  else
    shell.say "Sorry but some thing went wrong!"
    shell.say ""
    account.errors.full_messages.each { |m| shell.say "   - #{m}" }
  end

  shell.say ""
end

[
  {code: 346, order: 0, name: "福岡", prefix: "地方"},
  {code: 346, order: 1, name: "北九州", prefix: "地方"},
  {code: 346, order: 2, name: "筑豊", prefix: "地方"},
  {code: 346, order: 3, name: "筑後", prefix: "地方"},
  {code: 338, order: 0, name: "広島南部", prefix: "部"},
  {code: 338, order: 1, name: "広島南部", prefix: "部"},
  {code: 337, order: 0, name: "島根東部", prefix: "部"},
  {code: 337, order: 1, name: "島根西部", prefix: "部"},
  {code: 337, order: 2, name: "島根隠岐", prefix: "部"},
  {code: 318, order: 0, name: "千葉", prefix: "部"},
  {code: 318, order: 1, name: "銚子", prefix: "部"},
  {code: 318, order: 2, name: "館山", prefix: "部"},
].each do |land|
  begin
    Land.new(land).save
  rescue => e
    e.backtrace
  end
end

User.new({mail: "htty@localhost", hour: Time.now.hour, land_id: 3181}).save unless User.find(mail: "htty@localhost")

