Sequel.migration do
  up do
    create_table :lands do
      primary_key :id
      Int :code
      Int :order
      String :name
      String :prefix
    end
  end

  down do
    drop_table :lands
  end
end
