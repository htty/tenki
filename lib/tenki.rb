module Tenki

  TIME_SLOT = (0..24).map{|x|
    (:morning if x.between?(0,4) or x.between?(23,24)) ||
    (:afternoon if x.between?(5,10))||
    (:evening if x.between?(11,16))||
    (:night if x.between?(17,22))
  }

  module_function

  def cron
    users = User.where(:hour => Time.now.hour).all
    users.each do |user|
      send_mail(user, user.weather) if user.rain_threshold <= user.weather.max_rain
    end
  end

  def send_mail(user, w)
    mail = Mail.new do
      from    "tenki@nanja.net"
      to      user.mail
      subject "天気＠#{user.land.name}(#{w.month}月#{w.day}日#{w.hour}時)"
      body    "#{w.tenki_s} #{w.temp_s}\n#{w.rain_s}"
    end
    mail.charset = "utf-8"
    mail.delivery_method :sendmail
    mail.deliver
  end

  def jma2weather(land_id)
    t = Time.now
    url = "http://www.jma.go.jp/jp/yoho/"
    file = (land_id / 10).to_s + ".html"
    html = gethtml(url + file).force_encoding("utf-8")

    if (TENKI_CONFIG["debug"])
      FileUtils.mkdir_p Padrino.root + "/log/tenki/"
      File.open(Padrino.root + "/log/tenki/#{"%04d"%t.year}#{"%02d"%t.month}#{"%02d"%t.day}-#{"%02d"%t.hour}-#{land_id}.html","w").write(html)
    end

    w = Weather.new(raw: html)
    tenkicode, mintemp, maxtemp, rain0006, rain0612, rain1218, rain1824 = html_parse(html, land_id) 
    w.update({weather: tenkicode, mintemp: mintemp, maxtemp:maxtemp, rain0006: rain0006, rain0612: rain0612, rain1218: rain1218, rain1824: rain1824, year: t.year, month: t.month, day: t.day, hour: t.hour, raw: nil, land: Land.find(id: land_id)})
    w.save
    w
  end

  def gethtml(addr)
    #URLを展開する正規表現
    %r|^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?| =~ addr
    host = $4
    path = $5
    h = Net::HTTP.new(host, 80)
    return h.get(path).read_body
  end
  
  def html_parse(html, land_id)
    #htmlをパースして，データを返す
  
    land = ""
    t = Time.now
    #thタグで区切る
    htmls = html.split('<th colspan="2" class="th-area">')
    piece = htmls[land_id % 10 + 1]
    
    #天気部分を抜き出す
    if ((5 <= t.hour) && (t.hour < 17))
      piece =~ /<th class="weather">(.*?)<th class="weather">/m
      tmp = $1
    else
      piece =~ /<th class="weather">.*?<th class="weather">(.*?)<th class="weather">/m
      tmp = $1
    end
    #天気を調べる
    tmp =~ /<img src="img\/(.*?)\.png" align="middle".*?>/
    tenkicode = $1
    #最低/最高気温を調べる
    tmp =~ /<td class="min">(.*?)<\/td>/m
    if ($1 && $1 != "")
      mintemp = $1.gsub("\n","")
      mintemp = mintemp.gsub("度","")
    else
      mintemp = nil
    end
    tmp =~ /<td class="max">(.*?)<\/td>/m
    if ($1 && $1 != "")
      maxtemp = $1.gsub("\n","")
      maxtemp = maxtemp.gsub("度","")
    else
      maxtemp = nil
    end
    #降水確率を調べる
    if (tmp =~ /<td align="left".*?>00\-06<\/td>.*?<td align="right">(.*?)%<\/td>/m)
      rain0006 = $1
    else
      rain0006 = nil
    end
    if (tmp =~ /<td align="left".*?>06\-12<\/td>.*?<td align="right">(.*?)%<\/td>/m)
      rain0612 = $1
    else
      rain0612 = nil
    end
    if (tmp =~ /<td align="left".*?>12\-18<\/td>.*?<td align="right">(.*?)%<\/td>/m)
      rain1218 = $1
    else
      rain1218 = nil
    end
    if (tmp =~ /<td align="left".*?>18\-\d\d<\/td>.*?<td align="right">(.*?)%<\/td>/m)
      rain1824 = $1
    else
      rain1824 = nil
    end

    # 降水確率に--が入っていた場合の処理
    rain0006, rain0612, rain1218, rain1824 = [rain0006, rain0612, rain1218, rain1824].map{|x| (x.to_s =~ /\d+/) ? x : nil}
    result = [tenkicode, mintemp, maxtemp, rain0006, rain0612, rain1218, rain1824]
  end
  
end
