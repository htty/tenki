Tenki::Admin.controllers :weathers do
  get :index do
    @title = "Weathers"
    @weathers = Weather.all
    render 'weathers/index'
  end

  get :new do
    @title = pat(:new_title, :model => 'weather')
    @weather = Weather.new
    render 'weathers/new'
  end

  post :create do
    @weather = Weather.new(params[:weather])
    if (@weather.save rescue false)
      @title = pat(:create_title, :model => "weather #{@weather.id}")
      flash[:success] = pat(:create_success, :model => 'Weather')
      params[:save_and_continue] ? redirect(url(:weathers, :index)) : redirect(url(:weathers, :edit, :id => @weather.id))
    else
      @title = pat(:create_title, :model => 'weather')
      flash.now[:error] = pat(:create_error, :model => 'weather')
      render 'weathers/new'
    end
  end

  get :edit, :with => :id do
    @title = pat(:edit_title, :model => "weather #{params[:id]}")
    @weather = Weather[params[:id]]
    if @weather
      render 'weathers/edit'
    else
      flash[:warning] = pat(:create_error, :model => 'weather', :id => "#{params[:id]}")
      halt 404
    end
  end

  put :update, :with => :id do
    @title = pat(:update_title, :model => "weather #{params[:id]}")
    @weather = Weather[params[:id]]
    if @weather
      if @weather.modified! && @weather.update(params[:weather])
        flash[:success] = pat(:update_success, :model => 'Weather', :id =>  "#{params[:id]}")
        params[:save_and_continue] ?
          redirect(url(:weathers, :index)) :
          redirect(url(:weathers, :edit, :id => @weather.id))
      else
        flash.now[:error] = pat(:update_error, :model => 'weather')
        render 'weathers/edit'
      end
    else
      flash[:warning] = pat(:update_warning, :model => 'weather', :id => "#{params[:id]}")
      halt 404
    end
  end

  delete :destroy, :with => :id do
    @title = "Weathers"
    weather = Weather[params[:id]]
    if weather
      if weather.destroy
        flash[:success] = pat(:delete_success, :model => 'Weather', :id => "#{params[:id]}")
      else
        flash[:error] = pat(:delete_error, :model => 'weather')
      end
      redirect url(:weathers, :index)
    else
      flash[:warning] = pat(:delete_warning, :model => 'weather', :id => "#{params[:id]}")
      halt 404
    end
  end

  delete :destroy_many do
    @title = "Weathers"
    unless params[:weather_ids]
      flash[:error] = pat(:destroy_many_error, :model => 'weather')
      redirect(url(:weathers, :index))
    end
    ids = params[:weather_ids].split(',').map(&:strip)
    weathers = Weather.where(:id => ids)
    
    if weathers.destroy
    
      flash[:success] = pat(:destroy_many_success, :model => 'Weathers', :ids => "#{ids.to_sentence}")
    end
    redirect url(:weathers, :index)
  end
end
